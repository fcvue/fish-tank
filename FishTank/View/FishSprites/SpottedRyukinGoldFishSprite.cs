﻿using System.Drawing;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    /// RyukinGoldFish with a spot in the center
    /// </summary>
    class SpottedRyukinGoldFishSprite : RyukinGoldFishSprite
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedRyukinGoldFishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public SpottedRyukinGoldFishSprite(Fish fish)
            : base(fish)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedRyukinGoldFishSprite"/> class.
        /// </summary>
        /// <param name="fish"></param>
        /// <param name="direction"></param>
        public SpottedRyukinGoldFishSprite(Fish fish, int direction)
            : base(fish, direction)
        {
        }

        #endregion

        /// <summary>
        /// Paints the fish to face left
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintLeftFacingBody(Graphics graphics)
        {
            base.PaintLeftFacingBody(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintSpot(graphics, offsetX, offsetY);
        }

        /// <summary>
        /// Paints a fish to face right
        /// </summary>
        /// <param name="graphics"></param>
        public override void PaintRightFacingBody(Graphics graphics)
        {
            base.PaintRightFacingBody(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintSpot(graphics, offsetX, offsetY);
        }

        private void paintSpot(Graphics graphics, int offsetX, int offsetY)
        {
            var radius = 8.0;
            radius = radius*ScaleFactor;
            graphics.FillEllipse(Brushes.DarkOrange, (float) (offsetX + GetFishWidth()/2 - radius),
                (float) (offsetY + GetFishHeight()/2 - radius), (float) radius*2, (float) radius*2);
        }
    }
}
