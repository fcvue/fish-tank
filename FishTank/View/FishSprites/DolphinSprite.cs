﻿using System.Drawing;
using FishTank.Model.FishModel;
using FishTank.Properties;

namespace FishTank.View.FishSprites
{
    internal class DolphinSprite : PictureFishSprite
    {
        private readonly Bitmap eccoDolphinRight = new Bitmap(Resources.EccoDolphinRight);
        private readonly Bitmap eccoDolphinLeft = new Bitmap(Resources.EccoDolphinLeft);

        /// <summary>
        /// Creates a new fish sprite made from a picture
        /// </summary>
        public DolphinSprite()
        {
            TheFish = null;
            LeftFacingFish = new Bitmap(Resources.EccoDolphinLeft);
            RightFacingFish = new Bitmap(Resources.EccoDolphinRight);
        }

        /// <summary>
        /// Creates a new fish sprite made from a picture
        /// </summary>
        /// <param name="fish">the dolphin fish</param>
        public DolphinSprite(Fish fish)
            : base(fish)
        {
        }

        public override void PaintRightFacingBody(Graphics graphics)
        {
            graphics.DrawImage(this.eccoDolphinRight, TheFish.X, TheFish.Y, (float) (this.GetFishWidth()*ScaleFactor),
                (float) (this.GetFishHeight()*ScaleFactor));
        }

        public override void PaintLeftFacingBody(Graphics graphics)
        {
            graphics.DrawImage(this.eccoDolphinLeft, TheFish.X, TheFish.Y, (float) (this.GetFishWidth()*ScaleFactor),
                (float) (this.GetFishHeight()*ScaleFactor));
        }

        public override int GetFishWidth()
        {
            return this.eccoDolphinRight.Width;
        }

        public override int GetFishHeight()
        {
            return this.eccoDolphinRight.Height;
        }
    }
}
