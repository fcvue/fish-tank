﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Model.FishModel;

namespace FishTank.View.FishSprites
{
    /// <summary>
    ///     A SharkSprite is a kind of FishSprite that specifies a body shape for all sharks
    ///     Author: William Underwood
    ///     Version: 21 Sep 2015
    /// </summary>
    public class SharkSprite : DrawnFishSprite
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SharkSprite" /> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        /// <exception cref="ArgumentNullException">fish</exception>
        public SharkSprite(Fish fish) : base(fish)
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }
            TheFish = fish;

            this.setBodyCoordinates();
        }

        #endregion

        /// <summary>
        ///     Paints a shark's body and eye
        ///     Precondition: Graphics object cannot be null
        ///     Postcondition: Painted a black shark with a red eye
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void Paint(Graphics graphics)
        {
            PaintErrorCheck(graphics);
        }

        /// <summary>
        ///     Paints left facing body
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void PaintLeftFacingBody(Graphics graphics)
        {
            PaintErrorCheck(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintFishBody(graphics, FlipCoordinatesHorizontal(BodyCoordinates), offsetX, offsetY);
            DrawLeftFacingEye(graphics, new SolidBrush(Color.Red), offsetX, offsetY);
        }

        /// <summary>
        ///     Paints a fish to face right
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public override void PaintRightFacingBody(Graphics graphics)
        {
            PaintErrorCheck(graphics);
            var offsetX = TheFish.X;
            var offsetY = TheFish.Y;
            this.paintFishBody(graphics, BodyCoordinates, offsetX, offsetY);
            DrawRightFacingEye(graphics, new SolidBrush(Color.Red), offsetX, offsetY);
        }

        private void paintFishBody(Graphics graphics, Point[] coordinates, int offsetX, int offsetY)
        {
            var blackBrush = new SolidBrush(Color.Black);
            AdjustedCoordinates = coordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();
            graphics.FillPolygon(blackBrush, AdjustedCoordinates);
        }

        private void setBodyCoordinates()
        {
            BodyCoordinates = new[]
            {
                new Point {X = 0, Y = 5},
                new Point {X = 30, Y = 15},
                new Point {X = 40, Y = 10},
                new Point {X = 50, Y = 7},
                new Point {X = 60, Y = 0},
                new Point {X = 70, Y = 7},
                new Point {X = 90, Y = 12},
                new Point {X = 100, Y = 15},
                new Point {X = 115, Y = 22},
                new Point {X = 130, Y = 25},
                new Point {X = 115, Y = 28},
                new Point {X = 100, Y = 35},
                new Point {X = 90, Y = 40},
                new Point {X = 70, Y = 43},
                new Point {X = 60, Y = 50},
                new Point {X = 50, Y = 43},
                new Point {X = 40, Y = 40},
                new Point {X = 30, Y = 35},
                new Point {X = 0, Y = 45},
                new Point {X = 15, Y = 25}
            };

            ResizeShape(BodyCoordinates);
        }
    }
}