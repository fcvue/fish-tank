﻿using System;
using FishTank.Model.FishModel;

namespace FishTank.Utility
{
    /// <summary>
    /// Creates fish for the FishTankManager
    /// </summary>
    public static class FishFactory
    {
        private enum FishType { Shark, TigerShark, RyukinGoldFish, SpottedRyukinGoldFish, Dolphin };

        /// <summary>
        /// Creates a random Fish
        /// </summary>
        /// <returns>A random fish</returns>
        public static Fish CreateAFish()
        {
            Fish fish;
            switch (selectRandomFishType())
            {
                case FishType.Shark:
                    fish = new Shark();
                    break;
                case FishType.TigerShark:
                    fish = new TigerShark();
                    break;
                case FishType.RyukinGoldFish:
                    fish = new RyukinGoldFish();
                    break;
                case FishType.SpottedRyukinGoldFish:
                    fish = new SpottedRyukinGoldFish();
                    break;
                case FishType.Dolphin:
                    fish = new Dolphin();
                    break;
                default:
                    throw new Exception();
            }
            return fish;
        }

        private static FishType selectRandomFishType()
        {
            var values = Enum.GetValues(typeof (FishType));
            var randomFishType = (FishType) values.GetValue(Randomizer.GetNextRandom(values.Length));
            return randomFishType;
        }
    }
}
