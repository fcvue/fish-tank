﻿using System;

namespace FishTank.Utility
{
    /// <summary>
    /// Creates random numbers for fish type, location, and movement
    /// </summary>
    public static class Randomizer
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// Gets the next random number.
        /// </summary>
        /// <returns>Next random integer</returns>
        public static int GetNextRandom()
        {
            return Random.Next();
        }

        /// <summary>
        /// Gets the next non-negative random number lower than the upperbound.
        /// </summary>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns>Next random integer between 0 and upperBound inclusive</returns>
        public static int GetNextRandom(int upperBound)
        {
            return Random.Next(upperBound);
        }

        /// <summary>
        /// Returns the next random double between 0.0 and 1.0 inclusive
        /// </summary>
        /// <returns>random double between 0.0 and 1.0 inclusive</returns>
        public static double GetNextRandomDouble()
        {
            return Random.NextDouble();
        }

        /// <summary>
        /// Gets the next random double.
        /// </summary>
        /// <param name="lowerBound">The lower bound.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns>random double between bounds</returns>
        public static double GetNextRandomDouble(double lowerBound, double upperBound)
        {
            return lowerBound + Random.NextDouble()*(upperBound - lowerBound);
        }

        /// <summary>
        /// Gets the next random number within a specified range exclusive of the upper bound.
        /// </summary>
        /// <param name="lowerBound">The lower bound.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns>Next random integer between lowerBound and upperBound inclusive</returns>
        public static int GetNextRandom(int lowerBound, int upperBound)
        {
            return Random.Next(lowerBound, upperBound);
        }
    }
}
