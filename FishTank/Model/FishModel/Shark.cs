﻿using FishTank.Utility;
using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    /// <summary>
    ///     Models a shark
    /// </summary>
    public class Shark : Fish
    {
        /// <summary>
        ///     Creates a new shark
        /// </summary>
        public Shark()
        {
            MoveUpChance = .035;
            MoveDownChance = .035;
            MoveBackChance = .03;

            MinWeight = 12.0;
            MaxWeight = 45.0;

            Weight = Randomizer.GetNextRandomDouble(MinWeight, MaxWeight);
            Sprite = new SharkSprite(this);
        }
    }
}
