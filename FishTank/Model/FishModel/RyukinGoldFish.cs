﻿using FishTank.Utility;
using FishTank.View.FishSprites;

namespace FishTank.Model.FishModel
{
    /// <summary>
    /// This class models Ryukin Gold Fish
    /// </summary>
    public class RyukinGoldFish : Fish
    {
        /// <summary>
        /// Creates a new Ryukin gold fish
        /// Precondition - none
        /// Postcondition - a new Ryukin gold fish with a 
        ///     Ryukin gold fish sprite was created
        /// </summary>
        public RyukinGoldFish()
        {
            MoveUpChance = .15;
            MoveDownChance = .15;
            MoveBackChance = .20;

            MinWeight = .25;
            MaxWeight = 2.5;

            Weight = Randomizer.GetNextRandomDouble(MinWeight, MaxWeight);
            Sprite = new RyukinGoldFishSprite(this);
        }
    }
}
